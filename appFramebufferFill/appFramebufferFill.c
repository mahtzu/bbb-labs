/*
 ***************************************************************************
 * \brief   Embedded Linux Framebuffer Exercise 1.0
 *	    Basic framebuffer application.
 *	    Only a minimal error handling is implemented.
 * \file    appFrameBufferFill.c
 * \version 1.0
 * \date    28.09.2008
 * \author  Martin Aebersold
 *
 * \remark  Last Modifications:
 * \remark  V1.0, AOM1, 28.10.2013
 ***************************************************************************
 */

/* Declare the function prototypes headers */
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>

#include <linux/fb.h>

#include <sys/mman.h>
#include <sys/ioctl.h>

/************************************************************************/
/* MACROS								*/
/************************************************************************/

#define CONVERT_RGB24_16BPP(red, green, blue) \
	    (((red>>3)<<11) | ((green>>2)<<5) | (blue>>3))

/************************************************************************/
/* CONSTANTS								*/
/************************************************************************/

#define	BPP16	16

/*
 * Color definitions for 16-BPP	  	      R    G    B
 */

#define BLACK_16BPP	CONVERT_RGB24_16BPP(  0,   0,   0)
#define RED_16BPP	CONVERT_RGB24_16BPP(255,   0,   0)
#define GREEN_16BPP	CONVERT_RGB24_16BPP(  0, 255,   0)
#define YELLOW_16BPP	CONVERT_RGB24_16BPP(255, 255,   0)
#define BLUE_16BPP	CONVERT_RGB24_16BPP(  0,   0, 255)
#define MAGENTA_16BPP	CONVERT_RGB24_16BPP(255,   0, 255)
#define CYAN_16BPP	CONVERT_RGB24_16BPP(  0, 255, 255)
#define GREY_16BPP	CONVERT_RGB24_16BPP(192, 192, 192)
#define WHITE_16BPP	CONVERT_RGB24_16BPP(255, 255, 255)

/*
 ***************************************************************************
 * Define some data types
 ***************************************************************************
 */

struct RGB_COLOR
 {
  uint8_t r;
  uint8_t g;
  uint8_t b;
 };

/************************************************************************/
/* VARS									*/
/************************************************************************/

/* Screen Info */
struct fb_var_screeninfo fbVarScreenInfo;

uint16_t y;
uint16_t x;

int16_t  *pfb16;

uint32_t fbfd;
uint32_t screensize;

/*
 ******************************************************************************
 * main
 ******************************************************************************
 */

int main(int argc, char *argv[])
 {
  int32_t fbfd = 0;
  int32_t screensize = 0;

  printf("BBB Framebuffer Test\n");

  /*
   * Open device file for reading and writing
   */
  fbfd = open("/dev/fb0", O_RDWR);
  if (fbfd == -1)
   {
    perror("Error: cannot open framebuffer device");
    exit(errno);
   }
  printf("The framebuffer device was opened successfully.\n");

  /*
   * Get variable screen information
   */
  if (ioctl(fbfd, FBIOGET_VSCREENINFO, &fbVarScreenInfo) == -1)
   {
    perror("Error reading variable information");
    close(fbfd);
    exit(errno);
   }

  /* Figure out the size of the screen in bytes */
  screensize = (fbVarScreenInfo.xres * fbVarScreenInfo.yres * fbVarScreenInfo.bits_per_pixel) / 8;

  /*
   * Map the frame buffer device memory to user space.
   * Starting address in user space is pfb16.
   */
   if (fbVarScreenInfo.bits_per_pixel == BPP16)
    {
     pfb16 = (int16_t*) mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0);
     if (pfb16 == (int16_t*) -1)
      {
       perror("Error: failed to map 16-BPP framebuffer device to memory");
       exit(errno);
      }

     /* Fill the screen with 16 bpp, do it for all [x,y] pixel with the desired color	*/
     for (y=0; y<fbVarScreenInfo.yres; y++)
      {
       for (x=0; x<fbVarScreenInfo.xres; x++)
        {
         pfb16[x + y * fbVarScreenInfo.xres] = BLUE_16BPP;
        }
      }

     /* Cleanup */
     munmap(pfb16, screensize);
     close(fbfd);
    }
  return 0;
 }

