/*
 ***************************************************************************
 * \brief   Embedded-Android (BTE5484)
 *
 *  Android is providing a cool feature called Text to Speech (TTS) which
 *  speaks the text in different languages.
 *  This demo shows how to work with android text to speech or android
 *  speech synthesis.
 *
 * \file    TextToSpeechMainActivity.java
 * \version 1.0
 * \date    24.01.2014
 *
 * \remark  Last Modifications:
 * \remark  V1.0, AOM1, 24.01.2014   Initial release
 ***************************************************************************/

package ch.bfh.ti.texttospeechdemo;

import java.util.Locale;

import android.app.Activity;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.EditText;
import android.widget.Toast;

public class TextToSpeechMainActivity extends Activity implements OnInitListener
{

    private TextToSpeech tts;
    private Button btnSpeak;
    private EditText txtText;
    private Locale locale;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_to_speech_main);

        tts = new TextToSpeech(this, this);
        tts.setLanguage(Locale.US);

		/*
		 *  Refer 'Speak' button
		 */
        btnSpeak = (Button) findViewById(R.id.btnSpeak);

		/*
		 *  Refer 'Text' control
		 */
        txtText = (EditText) findViewById(R.id.txtText);

		/*
		 *  Handle onClick event for button 'Speak'
		 */
        btnSpeak.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
			/*
			 *  Run the method to speech the text
			 */
                speakOut();
            }
        });
    }

    @Override
    public void onInit(int status)
    {

        RadioButton radio = (RadioButton) findViewById(R.id.radioEn);
        radio.setChecked(true);

        // TODO Auto-generated method stub
	  /*
	   *  TTS is successfully initialized
	   */
        if (status == TextToSpeech.SUCCESS)
        {
		/*
		 *  Setting speech language
		 */
            int result = tts.setLanguage(Locale.US);

		/*
		 *  If your device doesn't support language you set above
		 */
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
            {
		  /*
		   *  Cook simple toast message with message
		   */
                Toast.makeText(this, "Language not supported", Toast.LENGTH_LONG).show();
                Log.e("TTS", "Language is not supported");
            }

		/*
		 *  Enable the button - It was disabled in main.xml (Go back and Check it)
		 */
            else
            {
                btnSpeak.setEnabled(true);
            }

		/*
		 *  TTS is not initialized properly
		 */
        }
        else
        {
            Toast.makeText(this, "TTS Initilization Failed", Toast.LENGTH_LONG)
                    .show();
            Log.e("TTS", "Initilization Failed");
        }
    }

    private void speakOut()
    {
      /*
       * Get the text typed
       */
        String text = txtText.getText().toString();

       /*
        * If no text is typed, tts will read out 'Welcome to the Android Course ... You have not entered any text'
        */
        if (text.length() == 0)
        {
            tts.speak("Welcome to the Android Course ... You have not entered any text", TextToSpeech.QUEUE_FLUSH, null);
        }
        else
        {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    public void onDestroy()
    {
      /*
       *  Don't forget to shutdown!
       */
        if (tts != null)
        {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioEn:
                if (checked)
                    Log.v("radio", "english selected");
                tts.setLanguage(Locale.US);
                break;
            case R.id.radioIt:
                if (checked)
                    Log.v("radio", "italian selected");
                tts.setLanguage(Locale.ITALIAN);
                break;
            case R.id.radioFr:
                if (checked)
                    Log.v("radio", "french selected");
                tts.setLanguage(Locale.FRENCH);
                break;
            case R.id.radioGe:
                if (checked)
                    Log.v("radio", "german selected");
                tts.setLanguage(Locale.GERMAN);
                break;
            default:
                Log.v("radio", "default selected");
                tts.setLanguage(Locale.US);
        }
    }
}