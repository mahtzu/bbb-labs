/****************************************************************************
 * Copyright (C) 2012 by Mathias Herzog                                     *
 *                                                                          *
 * This file is part of a BBB Lab project.                                  *
 *                                                                          *
 ****************************************************************************/

/**
 * @file bbbtemperature.h
 * @author mathu
 * @date 05 May 2017
 * @brief interface to the C-implementation for the temperature sensor
 * of the BBB-BFH-Case
 */

#ifndef BBBTEMPERATURE_H
#define BBBTEMPERATURE_H

#include <QTimer>
#include <QGraphicsView>

class BBBTemperature : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief BBBTemperature - definition for a temperature class
     */
    BBBTemperature(QGraphicsView *view);

public slots:
    /**
     * @brief BBBTemperatureSlot - call this slot to get new values
     * from the BBB temperautre sensor
     */
    void BBBTemperatureSlot();

signals:
    /**
     * @brief temperatureUpdate SIGNAL - will be triggered if a temperature
     * value is updated
     * @param new_value - new temperature value as int
     */
    void temperatureUpdate(int new_value);
    /**
     * @brief temperatureUpdate SIGNAL - will be triggered if a temperature
     * value is updated
     * @param new_value - new temperature value as string
     */
    void temperatureStringUpdate(QString new_value);

private:
    /**
     * @brief t_value - temperature value of BBB sensor
     */
    int t_value = 0;

    QGraphicsScene *scene;
    QGraphicsView *view;
};

#endif // BBBTEMPERATURE_H
