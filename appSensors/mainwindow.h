/****************************************************************************
 * Copyright (C) 2012 by Mathias Herzog                                     *
 *                                                                          *
 * This file is part of a BBB Lab project.                                  *
 *                                                                          *
 ****************************************************************************/

/**
 * @file mainwindow.h
 * @author mathu
 * @date 05 May 2017
 * @brief QT main window
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>

namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief MainWindow
     * @param parent
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

private:
    Ui::MainWindow *ui;

    QGraphicsScene *scene;
    QGraphicsEllipseItem *ellipse;
    QGraphicsRectItem *rectangle;
    QGraphicsTextItem *text;
};

#endif // MAINWINDOW_H
