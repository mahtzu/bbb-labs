#include "bbbtimer.h"
#include <QDebug>

#include "bbbtemperature.h"

int m_value = 0;

BBBTimer::BBBTimer(BBBTemperature *temperature)
{
    // create a timer
    timer = new QTimer(this);

    // setup signal and slot
    connect(timer, SIGNAL(timeout()),
          this, SLOT(BBBTimerSlot()));

    connect(timer, SIGNAL(timeout()),
            temperature, SLOT(BBBTemperatureSlot()));

    // msec
    timer->start(1000);
}

void BBBTimer::BBBTimerSlot()
{
    QString s = QString::number(++m_value);
    qDebug() << s;
    emit timerStringUpdate(s);
    emit timerUpdate(m_value);
}
