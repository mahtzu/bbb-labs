#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "bbbtimer.h"
#include "bbbthread.h"

#include "bbbtemperature.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    BBBTemperature *temperature = new BBBTemperature(ui->box_temp);

    // Create BBBTimer instance
    // QTimer object will be created in the BBBTimer constructor
    BBBTimer *timer = new BBBTimer(temperature);

    // Connect the Timer with a UI label
    connect(timer, SIGNAL(timerStringUpdate(QString)),
            ui->label_v_counter, SLOT(setText(QString)));

    // Connect the Temperature with a UI label
    connect(temperature, SIGNAL(temperatureStringUpdate(QString)),
            ui->label_v_temp, SLOT(setText(QString)));

    // Create some 2D graphics and show them on screen
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

    QBrush greenBrush(Qt::green);
    QPen outlinePen(Qt::black);
    outlinePen.setWidth(2);

    // addEllipse(x,y,w,h,pen,brush)
    ellipse = scene->addEllipse(0, -50, 50, 50, outlinePen, greenBrush);

}
MainWindow::~MainWindow()
{
    delete ui;
}

