#include "mainwindow.h"
#include <QApplication>

#include "bbbthread.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;

    QMainWindow window;

    // Threadding
    BBBThread thread1("A"), thread2("B"), thread3("C");

    thread1.start();
    thread2.start();
    thread3.start();


    w.show();

    return a.exec();
}
