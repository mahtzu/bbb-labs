/****************************************************************************
 * Copyright (C) 2012 by Mathias Herzog                                     *
 *                                                                          *
 * This file is part of a BBB Lab project.                                  *
 *                                                                          *
 ****************************************************************************/

/**
 * @file bbbtimer.h
 * @author mathu
 * @date 05 May 2017
 * @brief a timer implementation to handle frequent interface calls to
 * the BBB interfaces such as the temperature sensor
 */

#ifndef BBBTIMER_H
#define BBBTIMER_H

#include <QTimer>
#include "bbbtemperature.h"

class BBBTimer : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief BBBTimer - definition for the BBBTimer class
     * @param temperature
     */
    BBBTimer(BBBTemperature *temperature);
    /**
     * @brief timer - a QTimer variable
     */
    QTimer *timer;

public slots:
    /**
     * @brief BBBTimerSlot - will be called on every timer tick
     */
    void BBBTimerSlot();

signals:
    /**
     * @brief timerUpdate SIGNAL - will be called on every timer tick
     * @param new_val - new timer value as int
     */
    void timerUpdate(int new_val);
    /**
     * @brief timerStringUpdate SIGNAL - will be called on every timer tick
     * @param new_val - new timer value as string
     */
    void timerStringUpdate(QString new_val);

private:
    /**
     * @brief m_value increments on every timer interval
     */
    int m_value;
};

#endif // BBBTIMER_H
