#ifndef BBBTHREAD_H
#define BBBTHREAD_H

#include <QThread>
#include <QString>

class BBBThread : public QThread
{
public:
    // constructor
    // set name using initializer
    explicit BBBThread(QString s);

    // overriding the QThread's run() method
    void run();
private:
    QString name;
};

#endif // BBBTHREAD_H
