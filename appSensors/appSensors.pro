#-------------------------------------------------
#
# Project created by QtCreator 2017-05-09T21:03:17
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = appSensors
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    bbbthread.cpp \
    bbbtimer.cpp \
    bbbtemperature.cpp

HEADERS  += mainwindow.h \
    bbbthread.h \
    bbbtimer.h \
    bbbtemperature.h

FORMS    += mainwindow.ui

# Select architecture for native or cross
 linux-g++ {
 INCLUDEPATH += /home/student/Apps/Qt5.7.1/5.7/gcc_64/include/QtQwt
 INCLUDEPATH += /home/student/Apps/Qt5.7.1/5.7/gcc_64/include/QtWidgets
 INCLUDEPATH += /home/student/Apps/Qt5.7.1/5.7/gcc_64/include/QtOpenGL
 INCLUDEPATH += /home/student/Apps/Qt5.7.1/5.7/gcc_64/include/QtPrintSupport
 LIBS += -L/opt/Qt5.7.1/5.7/gcc_64/lib
} else:linux-arm-gnueabihf-g++ {
 INCLUDEPATH += /opt/gcc-linaro-6.3.1-2017.02-x86_64_arm-linux-gnueabihf/libc/usr/include
 INCLUDEPATH += /opt/embedded/bbb/rootfs/usr/include
 INCLUDEPATH += /opt/embedded/bbb/rootfs/usr/local/include
 INCLUDEPATH += /opt/embedded/bbb/rootfs/usr/local/Qt5.7.0/include
 INCLUDEPATH += /opt/embedded/bbb/rootfs/usr/local/Qt5.7.0/include/QtQwt
 INCLUDEPATH += /opt/embedded/bbb/rootfs/usr/local/Qt5.7.0/include/QtWidgets
 INCLUDEPATH += /opt/embedded/bbb/rootfs/usr/local/Qt5.7.0/include/QtOpenGL
 INCLUDEPATH += /opt/embedded/bbb/rootfs/usr/local/Qt5.7.0/include/QtPrintSupport
 LIBS += -L/opt/crosstools/gcc-linaro-6.3.1-2017.02-x86_64_arm-linux-gnueabihf/arm-linux-gnueabihf/libc/usr/lib
 LIBS += -L/opt/embedded/bbb/rootfs/lib
 LIBS += -L/opt/embedded/bbb/rootfs/usr/lib
 LIBS += -L/opt/embedded/bbb/rootfs/usr/local/lib
 LIBS += -L/opt/embedded/bbb/rootfs/usr/lib/arm-linux-gnueabihf
 LIBS += -L/opt/embedded/bbb/rootfs/usr/local/Qt5.7.0/lib
 target.path = /usr/local/bin
 INSTALLS += target
}
