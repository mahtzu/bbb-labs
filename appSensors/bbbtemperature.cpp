#include <QGraphicsView>
#include <QGraphicsScene>
#include "bbbtemperature.h"

BBBTemperature::BBBTemperature(QGraphicsView *view)
{

    this->view = view;

    // Create some 2D graphics and show them on screen
    this->scene = new QGraphicsScene(this);
    this->view->setScene(scene);
    this->scene->setSceneRect(0,0,100,25);

}

void BBBTemperature::BBBTemperatureSlot()
{
    this->t_value = this->t_value-4;
    emit temperatureStringUpdate(QString::number(t_value));
    emit temperatureUpdate(t_value);

    QPen pen(Qt::black);
    pen.setWidth(2);

    this->scene->clear();

    if (this->t_value < 0) {
        this->scene->addRect(40+this->t_value, 0, this->t_value*-1, 25, pen, Qt::blue);
    } else {
        this->scene->addRect(40, 0, this->t_value, 25, pen, Qt::red);
    }

    this->scene->addRect(39, 0, 2, 25, pen, Qt::black);


}
