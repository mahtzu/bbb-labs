/*
 ***************************************************************************
 * \brief   Embedded-Linux (BTE5446)
 *	    Linux Sysfs GPIO Exercise 1.0, Template
 *          Use this template for your apps and adjust it accordingly.
 * \file    appSysfsTemplate.c
 * \version 1.0
 * \date    25.10.2013
 * \author  Martin Aebersold
 *
 * \remark  Last Modifications:
 * \remark  V1.0, AOM1, 25.10.2013   Initial release
 * \remark  V1.1, AOM1, 20.11.2015   Added POSIX Timer Handling
 ***************************************************************************
 *
 * Copyright (C) 2013 Martin Aebersold, Bern University of Applied Scinces
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* Declare the function prototypes headers */
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

/* MCP9800 Register pointers */
#define MCP9800_TEMP     0x00      /* Ambient Temperature Register */
#define MCP9800_CONFIG   0x01      /* Sensor Configuration Register */
#define MCP9800_12_BIT   0x60      /* Select 12 Bit conversion  */

/* i2c Address of MCP9800 device */
#define MCP9800_I2C_ADDR (0x48)

int main() {
  /* File handle to i2cdev */
  int i2c_fd;
  int temperature = 0;

  /* Communication buffer */
  char i2cCommBuffer[2];

  /* Open the Linux i2c device */
  i2c_fd = open("/dev/i2c-2", O_RDWR);

  /* Set the I2C slave address for all subsequent I2C device transfers */
  ioctl(i2c_fd, I2C_SLAVE, MCP9800_I2C_ADDR);

  /* Setup i2c buffer for the config register */
  i2cCommBuffer[0] = MCP9800_CONFIG;
  i2cCommBuffer[1] = MCP9800_12_BIT;
  if (write(i2c_fd, i2cCommBuffer, 2) != 2) {
    perror("config write error");
    exit(2);
  }

  /* Setup MCP9802 register to read the temperature */
  i2cCommBuffer[0] = MCP9800_TEMP;

  /* Write data to i2c device */
  if (write(i2c_fd, i2cCommBuffer, 1) != 1) {
    perror("write error");
    exit(1);
  }

  /* Read the current temperature from the mcp9800 device */
  if (read(i2c_fd, i2cCommBuffer, 2) != 2) {
    perror("read error");
    exit(1);
  }

  /* Assemble the temperature values */
  temperature = ((i2cCommBuffer[0] << 8) | i2cCommBuffer[1]);
  temperature = temperature >> 4;

  printf("actual temperature: %f", temperature*0.0625);

  close(i2c_fd);
}
