package ch.bfh.ti.texttospeechdemo;

import android.util.Log;
import java.io.IOException;

/**
 * Created by mathu on 7/10/17.
 */

// define GPIO_LED1       61
// define GPIO_LED2       44
// define GPIO_LED3       68
// define GPIO_LED4       67
// define GPIO_T1         49
// define GPIO_T2         112
// define GPIO_T3         51
// define GPIO_T4         7

public class Led {

    public final String LED1 = "61";
    public final String LED2 = "44";
    public final String LED3 = "68";
    public final String LED4 = "67";

    private String TAG = "GPIO";

    public Led(String nr)
    {
        // initialize led

    }

    public void on() {
        // set led
    }

    public void off() {
        // unset led
    }
    public void destroy() {
        // if (mGpio != null) {
        //    try {
        //        mGpio.close();
        //        mGpio = null;
        //    } catch (IOException e) {
        //        Log.w(TAG, "Unable to close GPIO", e);
        //    }
        //}
    }

}