/*
 ***************************************************************************
 * \brief   Embedded-Android (BTE5484)
 *
 *  Android is providing a cool feature called Text to Speech (TTS) which
 *  speaks the text in different languages.
 *  This demo shows how to work with android text to speech or android
 *  speech synthesis.
 *
 * \file    TextToSpeechMainActivity.java
 * \version 1.0
 * \date    24.01.2014
 *
 * \remark  Last Modifications:
 * \remark  V1.0, AOM1, 24.01.2014   Initial release
 ***************************************************************************/

package ch.bfh.ti.texttospeechdemo;

import java.net.MalformedURLException;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Hashtable;

import android.app.Activity;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.EditText;
import android.widget.Toast;


public class TextToSpeechMainActivity extends Activity implements OnInitListener
{
    private TextToSpeech tts;
    private Button btnSpeak;
    private Button btnTemp;
    private EditText txtText;
    private AsyncHttpPost asyncHttpPost;
    private String inLang;
    private String outLang;

    /*
     * Define LEDs and Buttons
     */
    final String LED_L1 = "61";
    final String LED_L2 = "44";
    final String LED_L3 = "68";
    final String LED_L4 = "67";

    final String BUTTON_T1 = "49";
    final String BUTTON_T2 = "112";
    final String BUTTON_T3 = "51";
    final String BUTTON_T4 = "7";

    final String EN = "en";
    final String DE = "de";
    final String FR = "fr";
    final String CHI = "zh-CN";

    Hashtable lang = new Hashtable();
    Hashtable locale = new Hashtable();

    /* Timer task */
    Timer timer;
    MyTimerTask myTimerTask;

    /*
     * Define some useful constants
     */
    final char ON = '0';
    final char OFF = '1';

    final String PRESSED = "0";

    /*
     * Create new gpio object
     */
    final SysfsFileGPIO gpio = new SysfsFileGPIO();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_to_speech_main);

        tts = new TextToSpeech(this, this);
        tts.setLanguage((Locale)locale.get(EN));
        this.outLang = EN;
        this.inLang = EN;

        lang.put(EN, "English");
        lang.put(DE, "German");
        lang.put(FR, "French");
        lang.put(CHI, "Chineese");

        locale.put(EN, Locale.US);
        locale.put(DE, Locale.GERMAN);
        locale.put(FR, Locale.FRENCH);
        locale.put(CHI, Locale.CHINESE);

        timer = new Timer();
        myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 0, 1000);

        btnTemp = (Button) findViewById(R.id.btnTemp);
        btnSpeak = (Button) findViewById(R.id.btnSpeak);
        setInLang(EN);
        setOutLang(EN);

        txtText = (EditText) findViewById(R.id.txtText);

        /**
         * handle search button clicks
         */
        btnSpeak.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0) {
                translate(txtText.getText().toString());
            }
        });

        /**
         * handle temperature button clicks
         */
        btnTemp.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0) {
                switch (inLang) {
                    case DE:
                        translate("Die Temperatur beträgt 23 Grad");
                        break;
                    case EN:
                        translate("the temperature is 22 degree");
                        break;
                    case FR:
                        translate("c'est 45 degré");
                        break;
                    default:
                        translate("diese App ist absolute Spitzenklasse");
                }

            }
        });
    }

    /**
     * translate a given text using Microsoft cognitive services
     * @param text
     */
    public void translate(String text) {
        asyncHttpPost = new AsyncHttpPost(EN, DE);
        gpio.write_value(LED_L1, ON);
        asyncHttpPost.setListener(new AsyncHttpPost.Listener(){
            @Override
            public void onResult(String result) {
                Log.v("********** result", result);
                speakOut(result);
                gpio.write_value(LED_L1, OFF);
            }
        });
        asyncHttpPost.setText(text);
        asyncHttpPost.setInputLanguage(inLang);
        asyncHttpPost.setOutputLanguage(outLang);
        asyncHttpPost.execute();
    };

    @Override
    public void onInit(int status) {

        RadioButton radio = (RadioButton) findViewById(R.id.radioEn);
        radio.setChecked(true);

        gpio.write_value(LED_L1, OFF);
        gpio.write_value(LED_L2, OFF);
        gpio.write_value(LED_L3, OFF);
        gpio.write_value(LED_L4, OFF);

        if (status == TextToSpeech.SUCCESS) {
            int result = tts.setLanguage(Locale.US);
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Toast.makeText(this, "Language not supported", Toast.LENGTH_LONG).show();
                Log.e("TTS", "Language is not supported");
            }
            else {
                btnSpeak.setEnabled(true);
            }
            setInLang(EN);
            setOutLang(EN);
        }
        else {
            Toast.makeText(this, "TTS Initilization Failed", Toast.LENGTH_LONG).show();
            Log.e("TTS", "Initilization Failed");
        }
    }

    /**
     * speak a given text in a specific language
     * @param text
     */
    private void speakOut(String text) {
        if (text.length() == 0) {
            tts.speak("please enter a text", TextToSpeech.QUEUE_FLUSH, null);
        }
        else {
            if (outLang == CHI) {
                Toast.makeText(this, text, Toast.LENGTH_LONG).show();
            } else {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

    /**
     * set the input language
     * @param language
     */
    private void setInLang(String language) {
        Log.v("Language", language + " as input selected");
        inLang = language;
    }

    /**
     * set the output language
     * @param language
     */
    private void setOutLang(String language) {
        Log.v("Language", language + "as output selected");
        tts.setLanguage((Locale)locale.get(language));
        outLang = language;
        btnSpeak.setText("Speak " + lang.get(outLang));
    }

    /**
     * cleanup
     */
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        gpio.write_value(LED_L1, OFF);
        gpio.write_value(LED_L2, OFF);
        gpio.write_value(LED_L3, OFF);
        gpio.write_value(LED_L4, OFF);
        super.onDestroy();
    }

    /**
     * handle radio button clicks
     * @param view
     */
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.radioEn:
                if (checked)
                    setInLang(EN);
                    break;
            case R.id.radioCh:
                if (checked)
                    setInLang(CHI);
                    break;
            case R.id.radioFr:
                if (checked)
                    setInLang(FR);
                    break;
            case R.id.radioGe:
                if (checked)
                    setInLang(DE);
                    break;
            default:
                Log.v("radio", "default selected");
                tts.setLanguage((Locale)locale.get(EN));
        }
    }

    /**
     * implementation for a custom timer -> needed for button clicks
     */
    class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (gpio.read_value(BUTTON_T1).equals(PRESSED)) {
                        setOutLang(EN);
                    }
                    if (gpio.read_value(BUTTON_T2).equals(PRESSED)) {
                        setOutLang(DE);
                    }
                    if (gpio.read_value(BUTTON_T3).equals(PRESSED)) {
                        setOutLang(FR);
                    }
                    if (gpio.read_value(BUTTON_T4).equals(PRESSED)) {
                        setOutLang(CHI);
                    }
                }
            });
        }
    }
}