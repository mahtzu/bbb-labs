package ch.bfh.ti.texttospeechdemo;

import android.util.Log;

import java.io.IOException;
import javax.net.ssl.HttpsURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import org.apache.commons.io.IOUtils;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.StringReader;
import org.xml.sax.InputSource;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPath;

import android.os.AsyncTask;


public class AsyncHttpPost extends AsyncTask<String, String, String> {

    String key = "e7f24c064d3b494ebcf37715dca2ed6e";
    String authenticationUrl = "https://api.cognitive.microsoft.com/sts/v1.0/issueToken";
    String input = "kein Text eingegeben. Bitte gib einen Text zum übersetzen ein";
    String inLang = "en";
    String outLang = "en";


    interface Listener {
        void onResult(String result);
    }
    private Listener mListener;

    /**
     * constructor
     */
    public AsyncHttpPost(String inLang, String outLang) {
        this.inLang = inLang;
        this.outLang = outLang;
    }

    /**
     * set the text to translate
     * @param text
     */
    public void setText(String text) {
        this.input = text;
    }

    /**
     * set translation input language
     * @param lang
     */
    public void setInputLanguage(String lang) {
        this.inLang = lang;
    }

    /**
     * set translation output language
     * @param lang
     */
    public void setOutputLanguage(String lang) {
        this.outLang = lang;
    }

    /**
     * set listener
     */
    public void setListener(Listener listener) {
        mListener = listener;
    }

    /**
     * background
     */
    @Override
    protected String doInBackground(String... params) {
        byte[] result = null;
        String output = "####### no output";
        String token = "";
        try {

            Log.v("AsyncHTTPPost from", this.inLang);
            Log.v("AsyncHTTPPost to", this.outLang);

            HttpsURLConnection authConn = (HttpsURLConnection) new URL(authenticationUrl).openConnection();
            authConn.setRequestMethod("POST");
            authConn.setDoOutput(true);
            authConn.setRequestProperty("Ocp-Apim-Subscription-Key", key);
            IOUtils.write("", authConn.getOutputStream(), "UTF-8");
            token = IOUtils.toString(authConn.getInputStream(), "UTF-8");
            Log.v("httpPost", token);


            // Using the access token to build the appid for the request url
            String appId = URLEncoder.encode("Bearer "+token, "UTF-8");
            String text = URLEncoder.encode(this.input, "UTF-8");
            String from = this.inLang;
            String to = this.outLang;
            String translatorTextApiUrl = String.format("https://api.microsofttranslator.com/v2/http.svc/Translate?appid=%s&text=%s&from=%s&to=%s", appId, text, from, to);
            HttpsURLConnection translateConn = (HttpsURLConnection) new URL(translatorTextApiUrl).openConnection();
            translateConn.setRequestMethod("GET");
            translateConn.setRequestProperty("Accept", "application/xml");
            output = IOUtils.toString(translateConn.getInputStream(), "UTF-8");
        }

        catch (MalformedURLException e) {
            Log.v("httpPost error", e.toString());
        }

        catch (IOException e) {
            Log.v("httpPost error", e.toString());
        }

        catch (Exception e) {
            Log.v("httpPos errort", e.toString());
        }

        return parseResponse(output);
    }

    /**
     * on getting result
     */
    @Override
    protected void onPostExecute(String result) {
        // something...
        if (mListener != null) {
            mListener.onResult(result);
        }
    }

    /**
     * parse xml response from translation service
     * @param response
     * @return
     */
    public String parseResponse(String response) {

        try {
            InputSource source = new InputSource(new StringReader(response));

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(source);

            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();

            return xpath.evaluate("/string", document);
        } catch (Exception e) {
            Log.v("########", e.toString());
            return "keine Antwort vom Übersetzungsdienst";
        }
    }
}
