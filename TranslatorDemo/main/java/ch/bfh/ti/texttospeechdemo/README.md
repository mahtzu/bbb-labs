# TranslatorDemo 

This project is a simple Android app do demonstrate Microsoft cloud text translation service on a BBB BFH-CAPE.
The App translates a Text form one language into another. Additionally the text will be transformed from text to speach.

Supported Languages for input and output are
* English
* German
* French
* Chineese

**Note** Chineese is not supported by the speech system of the BFH BBB KitKat version. If you translate to Chineese, a Toast will be occur instead of a speaker

## Getting Started

These instructions will get you a copy of the project up and running on your BBB for development purposes. 

### Prerequisites

Before you can start, make sure that following prerequisits are met

* A running Android studio 2.3.3 on a Linux develompment machine
* A BBB-BFH-CAPE, connected to the Linux System that running the Android studio
* Android KitKat has to be up and running on the BBB
* **IMPORTANT**: Your Android has to be connected to the internet!!


### Installing and Deployment

* Open the project folder in your Android Studio
* Connect your BBB to your Linux development machine
* Build an run the App from within Android Studio

## Manual

### Basic functions

When the app is started on your BBB, execute the following steps:

* Set the desired input language by selecting one of the radio buttons at the bottom left 
* In the textfield in the middle of the app, type your text that you want to be translated. Use the same language for the text that you've set with the radio button
* Press the buttons T1 to T4 to select the output language for your text. You'll see the selected language on the button label, i.e. "SPEAK GERMAN" if you select T2 to tanslate to German
* Now hit the large button in the mittle of the App. It can take a few seconds to translate your text

### Additional functionality
* The translation will be done by Microsoft Cognitive Cloud services. During the translation request to the cloud, the LED 1 indicates that the request is  processing
* Hit the "Temperature" button on the bottom right to get the current temperature. The output language corresponds to the language that was set with T1 to T4

## Troubleshoot
Nothing happens if you hit the SPEAK button
```
Make sure that your BBB is connected to the internet
```

In any case of a failure
```
Connect to your BBB using the `adb shell`command and consult the log files with the command `logcat`
```

## Authors

* **Mathias Herzog, mathu at gmx dot ch** - *Initial work*

## License

This project is licensed under the GPL License see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Inspired by code from Martin Aebersold, BFH-TI
