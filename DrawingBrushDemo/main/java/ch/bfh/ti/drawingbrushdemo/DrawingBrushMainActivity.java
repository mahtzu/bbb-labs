/*
 ***************************************************************************
 * \brief   Embedded-Android (BTE5484)
 *
 * 			Drawing Brush Application.
 *	       	Draw anything on the screen just by touching and move your finger.
 *	        Clear your screen by pressing the “Clear Screen” button.
 *
 * \file    DrawingBrushMainActivity.java
 * \version 1.0
 * \date    24.01.2014
 *
 * \remark  Last Modifications:
 * \remark  V1.0, AOM1, 24.01.2014   Initial release
 ***************************************************************************/

package ch.bfh.ti.drawingbrushdemo;

import android.os.Bundle;
import android.app.Activity;

import android.view.Gravity;
import android.view.Menu;
import android.widget.Button;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.graphics.Color;

public class DrawingBrushMainActivity extends Activity
 {
  @Override
  public void onCreate(Bundle savedInstanceState)
   {
	super.onCreate(savedInstanceState);
	DrawingBrushTouchEventView tv = new DrawingBrushTouchEventView(this);

       setContentView(tv);

       tv.setBackgroundColor(0xFF000000);
       //addContentView(tv.btnReset, tv.params);
       addContentView(tv.linLayout, tv.linLayoutParams);

  }


 @Override
 protected void onPause()
  {
   // TODO Auto-generated method stub
   super.onPause();
   finish();
  }
 }
