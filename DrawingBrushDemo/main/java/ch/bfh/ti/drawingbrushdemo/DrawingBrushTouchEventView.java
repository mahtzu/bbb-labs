package ch.bfh.ti.drawingbrushdemo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringDef;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.util.*;
import android.widget.RelativeLayout;
import android.view.Menu;

public class DrawingBrushTouchEventView extends View
 {
	 private Paint paint = new Paint();
	 private Path path = new Path();
	 private Paint circlePaint = new Paint();
	 private Path circlePath = new Path();

	 public Button btnReset;
	 public Button btnRed;
	 public Button btnBlue;
	 public Button btnGreen;
	 public LayoutParams params;

	 public LinearLayout linLayout;
	 public LayoutParams linLayoutParams;

	 public int RED = 0xAAF84364;
	 public int REDCLK = 0xFFF84364;
	 public int BLUE = 0xAA79A8EA;
	 public int BLUECLK = 0xFF79A8EA;
	 public int GREEN = 0xAA2cf9a3;
	 public int GREENCLK = 0xFF2cf9a3;


  public DrawingBrushTouchEventView(Context context)
   {
	super(context);

	/*
	 * Set paint attributes
	 */
	paint.setAntiAlias(true);
	paint.setColor(BLUECLK);
	paint.setStyle(Paint.Style.STROKE);
	paint.setStrokeJoin(Paint.Join.ROUND);
	paint.setStrokeWidth(15f);


	/*
	 * Set circle attributes
	 */
	circlePaint.setAntiAlias(true);
	circlePaint.setColor(0xFFeee587);
	circlePaint.setStyle(Paint.Style.STROKE);
	circlePaint.setStrokeJoin(Paint.Join.MITER);
	circlePaint.setStrokeWidth(4f);

	   /*
		* Create buttons
		*/
	   btnReset = new Button(context);
	   btnReset.setBackgroundColor(0xFFFFFFFF);
	   btnReset.setText("Clear Screen");

	   btnRed = new Button(context);
	   btnRed.setBackgroundColor(RED);
	   btnRed.setText("Red");

	   btnBlue = new Button(context);
	   btnBlue.setBackgroundColor(BLUECLK);
	   btnBlue.setText("Blue");

	   btnGreen = new Button(context);
	   btnGreen.setBackgroundColor(GREEN);
	   btnGreen.setText("Green");


	   linLayout = new LinearLayout(context);
	   linLayout.setOrientation(LinearLayout.HORIZONTAL);
	   linLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

	   LayoutParams lpView = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

	   linLayout.addView(btnReset, lpView);
	   linLayout.addView(btnRed, lpView);
	   linLayout.addView(btnBlue, lpView);
	   linLayout.addView(btnGreen, lpView);


	btnReset.setOnClickListener(new View.OnClickListener()
	 {
	  public void onClick(View v)
	   {
		// TODO Auto-generated method stub

	    /*
	     *  resets the screen
	     */
		path.reset();

		/*
		 *  Calls the onDraw() method by invalidating the screen
		 */
		postInvalidate();

	   }
	});



	// blueButton = (Button) findViewById(R.id.button_id);
	btnRed.setOnClickListener(new View.OnClickListener() {
	   public void onClick(View v) {
		   paint.setColor(REDCLK);
		   btnRed.setBackgroundColor(REDCLK);
		   btnBlue.setBackgroundColor(BLUE);
		   btnGreen.setBackgroundColor(GREEN);
		   Log.v("color button", "red clicked");
		   // Code here executes on main thread after user presses button
	   }
	});

   btnBlue.setOnClickListener(new View.OnClickListener() {
	   public void onClick(View v) {
		   paint.setColor(BLUECLK);
		   btnBlue.setBackgroundColor(BLUECLK);
		   btnRed.setBackgroundColor(RED);
		   btnGreen.setBackgroundColor(GREEN);
		   Log.v("color button", "red clicked");
		   // Code here executes on main thread after user presses button
	   }
   });

   btnGreen.setOnClickListener(new View.OnClickListener() {
	   public void onClick(View v) {
		   paint.setColor(GREENCLK);
		   btnGreen.setBackgroundColor(GREENCLK);
		   btnRed.setBackgroundColor(RED);
		   btnBlue.setBackgroundColor(BLUE);
		   Log.v("color button", "red clicked");
		   // Code here executes on main thread after user presses button
	   }
   });

  }



  @Override
  protected void onDraw(Canvas canvas)
   {
	canvas.drawPath(path, paint);
	canvas.drawPath(circlePath, circlePaint);
   }

  @Override
  public boolean onTouchEvent(MotionEvent event)
   {
	/*
	 *  Gives you x and y coordinates on the Event.
	 */
	float pointX = event.getX();
	float pointY = event.getY();

	/*
	 *  Checks for the event that occurs
	 */
	switch (event.getAction())
	 {
	  case MotionEvent.ACTION_DOWN:
		path.moveTo(pointX, pointY);
	  return true;
	  
	  case MotionEvent.ACTION_MOVE:
		path.lineTo(pointX, pointY);
		circlePath.reset();

		/*
		 *  (circle's center x-coordinate, y-coordinate, radius of the circle, direction to wind the shape)
		 */
		circlePath.addCircle(pointX, pointY, 30, Path.Direction.CW);
	   break;

	   case MotionEvent.ACTION_UP:
		circlePath.reset();
	   break;
	  
	   default:
		return false;
	  }

	/*
	 *  Schedules a repaint. Force a view to draw.
	 */
	postInvalidate();
	return true;
   }

 }
